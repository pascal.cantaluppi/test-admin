import './App.css';
import Navigation from './components/Navigation';
import Grid from './components/Grid';
import Form from './components/Form';

function App() {
  return (
    <div className="App">
      <Navigation />
      <p />
      <Grid />
      <p />
      <Form />
    </div>
  );
}

export default App;
