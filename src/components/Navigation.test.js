import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import Navigation from './Navigation';

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders component', () => {
  // eslint-disable-next-line testing-library/no-unnecessary-act
  act(() => {
    render(<Navigation />, container);
  });
  expect(container.textContent).toBe('Admin Panel');
});
