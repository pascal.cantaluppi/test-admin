# Automated Testing | Admin

React Dashboard for Component-testing

[Picture - Logo]

## Description

This Project ist part of the automatest testing guidelines written by Pascal Cantaluppi.

It contains the testing procedures for React frontend components.

[Picture - Jest]

## Installation

This project uses [Node.js](https://nodejs.org/) as the JavaScript runtime.

```
git clone https://gitlab.com/pascal.cantaluppi/test-frontend.git
cd test-frontend
npm i
npm start
```

## Testing

Run automated tests
```
npm test
```

Update Snapshots
```
npm test --updateSnapshot
```

Coverage
```
npm test --collectCoverage
```

## Deployment

The ci/cd pipeline is runnig the test suite as defined in the Dockerfile:

[Picture - Dockerfile]

Preview:

[https://admin.cantaluppi.org](https://admin.cantaluppi.org)

