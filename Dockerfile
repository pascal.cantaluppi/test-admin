# build environment
FROM node:17-alpine as builder
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build

# production environment
FROM node:17-alpine
RUN npm i -g serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "3000", "-s", "."]
